![Bild 1](media/1.jpeg)

**Illustration av ett urval av tomter, olika trafikslag samt topograf.**

# Bättre analys av byggprojektmed hjälp av BIM och GIS

> ##### I dagens samhällsplanering skulle det vara till stor fördel att använda geografiskt bunden information och byggnadsknuten information i samma analysmodell när man ska utvärdera förutsättningar för planerade projekt. Ett OpenBIM-projekt har lagt grunden till en upprepningsbar analysmodell som innefattar sociala, ekologiska och ekonomiska parametrar och som baserar sig på delaktighet i processen.

INFÖR ALLA PLANLÄGGNINGS- OCH BYGGPROCESSER måste man göra en analys. Ibland görs denna ingående och noggrant med ett tydligt mål, men ofta gäller motsatsen – idéer och tyckanden är inte alltid helt underbyggda. Det händer ofta att en idé snabbt får fäste utan att man först har gjort en ordentlig analys. Resultatet kan bli att exempelvis en hel detaljplaneprocess måste göras om. 
​	Det behövs en konkret tjänst i form av en analysmetod för tidiga skeden. Arkitekten Maria Grunditz har tillsammans med sina kollegor i Projektengagemang, energi- och klimatexperten Lisa Engqvist och geografen Karl Maandi, genomfört projektet Bebyggelseanalys, som syftar till att utveckla en sådan metod med BIM- och GIS-parametrar i samverkan. Projektet har gjorts på uppdrag av OpenBIM.
​	I dag saknas ingenting för att kunna göra en bra och omfattande bebyggelseanalys men de kostar ofta för mycket för att alltid göras i de lägen de behövs. De stora kommunerna har råd att låta konsulter göra stora djuplodande analyser men mindre kommuner har inte råd med sådana.
​	– Planeringsprocessen i dagens samhälle är komplex och behöver mer förfinade planeringsinstrument än tidigare, säger Maria Grunditz. När byggherrar kommer med idéer och förslag är det viktigt att kommunen kan värdera förslagen och studera dem ur olika perspektiv. Vi tänker oss en metod som man kan hålla sig till och som är upprepningsbar, tydlig, lättförståelig och strukturerad. 
​	Det finns idag olika analysmetoder som både är strukturerade och upprepningsbara, men ingen metod som omfattar både GIS och BIM. Modeller med kopplingar mellan GIS- och BIM-parametrar är i praktiken ett outforskat område.
Utmaningarna är stora – kunskapskraven ökar, planeringssammanhangen blir alltmer komplexa, hållbarhetsmålen är högprioriterade och det är svårt att samordna intressen från samhälle, byggherre och brukare. 
​	GIS står för geografiska informationssystem (datatekniska lösningar) som hanterar geografiska data. Det finns oändligt med information som är geografiskt bunden, till exempel sådant som rör sociala, ekonomiska och geografiska frågor.

Tanken är att den som beställer ett uppdrag också väljer inriktning på vilka frågor som är viktigast att beakta.

I SVERIGE FINNS INGEN SAMLAD GEOGRAFISK databas, utan man får samla ihop data själv från olika informationshållare. Lantmäteriet disponerar en del av materialet ( till exempel Fastighetskartan, höjddata) medan annan information saluförs av olika
företag.
​	– I projektet hade vi svårt att få tag på information eftersom vi inte kunde hänvisa till ett skarpt projekt utan bara gjorde en fallstudie. Då har man inte tillgång till så mycket fri information. Utvecklingen i Sverige och flera andra länder bromsas av
att mycket data inte står till allmänt förfogande.. Olika GIS- och BIM-parametrar påverkar varandra och samverka ibland. GIS-parametrar är exempelvis vindriktning, vegetation och tomtens lutning. BIM-parametrar beskriver hur huset är uppbyggt, exempelvis form, kulör och takbeklädnad. Ett sedumtak kan till exempel kopplas till klimatologisk information 
​	– Tanken är att kunna laborera med olika parametrar, att koppla byggnaden till olika GIS-parametrar och se hur de samverkar. Det handlar mest om strategiskt tänkande, att använda de fakta som finns för att skapa trygga miljöer och bra sammanhang.

ARBETSMETODEN SKA VARA SNABB, lättanvänd och ekonomisk, en enkel metod att ta till i en komplex valsituation.

• **Målformulering**. En tydlig målformulering för analysen är A och O för ett användbart resultat och målen bör vara mätbara. Transparens och kommunikation ska ingå kring målformuleringen.
• **Kriterieval**. Definierade kvalitetsaspekter lyfts fram – de väljs, beskrivs och viktas. Ekonomiska konsekvenser tydliggörs och väljs, beskrivs och viktas. Kriterieval genomförs lämpligen i workshopform med alla berörda, så att de som ska erhålla resultatet är fullständigt införstådda med hur det har uppkommit.
• **Byggprojekt** utformas enligt ett känt miljöcertifieringssystem. Här finns olika system att välja på som Leeds, BREEAM och Green Building. Valet optimeras utifrån beställarens behov.
• **Analys**. Analysen genomförs med skriftliga och tydliga motiveringar för val gjorda i kriteriesammanställningen.
• **Uppföljning**. Uppföljning och dokumentation av resultatet samt erfarenhetsåterföring är viktiga delar av metoden. Idag försvinner mycket kunskap i bland annat generationsskiften, vilket kan motverkas med ett systematiskt dokumenterande verktyg.
​	– Metoden innebär en sammanställning av viktiga programfrågor, och en analys av dessa. Därefter gör man en illustrerande överlagring av data i form av kartor. Valet av kriterier är det allra svåraste – att välja och vikta kriterier är en subjektiv
bedömning och man medverkar själv till svaret.

EN FALLSTUDIE SOM GJORDES INOM PROJEKTET visar att metoden fungerar. Utifrån olika kriterier fanns fem tomter att välja bland för att anlägga ett studentboende. Analys av tomterna visade att en av tomterna var lämpligast. Tre grundformer av byggnaden ritades och utsattes för energianalys i IDA.
​	– Valet av huskropp var det mest överraskande, säger Maria Grunditz. Analysen visade att den byggnadstyp vi trodde minst på faktiskt var den lämpligaste. I fallstudien såg vi att antaganden kan verifieras eller avskrivas genom enkla analyser med GIS-och BIM-parametrar.

INGET HINDRAR IDAG SAMORDNADE STUDIER av GIS- och BIM-parametrar i en analys, men få har undersökt sambandet. Maria Grunditz tror att det beror på att presumtiva användare inte har insikten om att det lönar sig att göra sådana studier.
​	En intressant fråga är hur man kan lägga in GIS-parametrar i byggnadsinformationsmodellen. Idag finns troligtvis ingen programvara som klarar det, men det hindrar inte att man använder metoden ändå och låter de olika parametrarna samverka.
​	– Vi vill kunna utveckla metoden så att man kan erbjuda den till kommuner och byggherrar. I projektet visar vi på behovet och möjligheterna. Att utveckla metoden och få ett väl fungerande verktyg ligger i allas intresse och skulle vara av stor samhällsekonomisk nytta.